fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut sum  = 0;
	for n in 1..(number-1) {
        if n % multiple1 == 0 || n % multiple2 == 0 {
            sum += n;
        }
    }
    sum
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
